package com.hw.db.controllers;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

class threadControllerTests {

    private Thread thread;
    private List<Post> posts;
    private List<User> users;
    private User user;
    private Vote vote;


    @BeforeEach
    @DisplayName("forum creation test")
    void createForumTest() {
        thread = new Thread("mikhailGudkov", Timestamp.valueOf("1234-12-12 12:12:12"), "forum", "message", "slug", "title", 3);
        thread.setId(1);
        posts = new ArrayList<>();
        users = new ArrayList<>();
        user = new User("some", "some@email.mu", "name", "nothing");
        posts.add(new Post("author", Timestamp.valueOf("1234-12-12 12:12:12"), "forum", "message", 1, 2, false));
        users.add(user);
        vote = new Vote("vote",1);
    }

    @Test
    public void CheckIdOrSlugWithSlugTest() {
        MockedStatic<ThreadDAO> threadDao = Mockito.mockStatic(ThreadDAO.class);
            threadDao.when(() -> ThreadDAO.getThreadBySlug("some"))
                    .thenReturn(thread);
            try (MockedStatic forumDAO = Mockito.mockStatic(ForumDAO.class)) {
                threadController threadController = new threadController();
                Thread threadTest = threadController.CheckIdOrSlug("some");
                assertEquals(thread, threadTest);
            }
            threadDao.close();
    }

    @Test
    public void CheckIdOrSlugWithIdTest() {
        MockedStatic<ThreadDAO> threadDao = Mockito.mockStatic(ThreadDAO.class);
        threadDao.when(() -> ThreadDAO.getThreadById(1234))
                .thenReturn(thread);
        try (MockedStatic forumDAO = Mockito.mockStatic(ForumDAO.class)) {
            threadController threadController = new threadController();
            Thread threadTest = threadController.CheckIdOrSlug("1234");
            assertEquals(thread, threadTest);
        }
        threadDao.close();
    }

    @Test
    public void createPostTest() {
        MockedStatic<ThreadDAO> threadDao = Mockito.mockStatic(ThreadDAO.class);
        List<Post> expectedPosts = new ArrayList<>();
        Post expectedPost = new Post("some", Timestamp.valueOf("1234-12-12 12:12:12"), "forum", "message", 1, 1, false);
        expectedPost.setId(1);
        expectedPosts.add(expectedPost);
        MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class);
        threadDao.when(() -> ThreadDAO.getThreadBySlug("some"))
                .thenReturn(thread);
        userMock.when(() -> UserDAO.Info("author")).thenReturn(user);
        threadController threadController = new threadController();
        ResponseEntity responseEntity = threadController.createPost("some", posts);
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(((List<Post>) responseEntity.getBody()).get(0).getAuthor(), expectedPost.getAuthor());
        assertEquals(((List<Post>) responseEntity.getBody()).get(0).getMessage(), expectedPost.getMessage());
        assertEquals(((List<Post>) responseEntity.getBody()).get(0).getForum(), expectedPost.getForum());
        assertEquals(((List<Post>) responseEntity.getBody()).get(0).getCreated(), expectedPost.getCreated());
        assertEquals(((List<Post>) responseEntity.getBody()).get(0).getParent(), expectedPost.getParent());
        assertEquals(((List<Post>) responseEntity.getBody()).get(0).getThread(), expectedPost.getThread());
        threadDao.close();
        userMock.close();
    }

    @Test
    public void postsTest() {
        MockedStatic<ThreadDAO> threadDao = Mockito.mockStatic(ThreadDAO.class);
        List<Post> expectedPosts = new ArrayList<>();
        Post expectedPost = new Post("some", Timestamp.valueOf("1234-12-12 12:12:12"), "forum", "message", 1, null, false);

        threadDao.when(() -> ThreadDAO.getThreadById(1))
                .thenReturn(thread);
        threadDao.when(() -> ThreadDAO.getPosts(1, 1, 2, "sort", false))
                .thenReturn(expectedPosts);
        threadController threadController = new threadController();

        ResponseEntity responseEntity = threadController.Posts("1", 1, 2, "sort", false);
        assertEquals(ResponseEntity.status(HttpStatus.OK).body(expectedPosts), responseEntity);
        threadDao.close();
    }

    @Test
    public void changeTest() {
        MockedStatic<ThreadDAO> threadDao = Mockito.mockStatic(ThreadDAO.class);
        threadDao.when(() -> ThreadDAO.getThreadById(1))
                .thenReturn(thread);
        //threadMock.when(() -> ThreadDAO.change(any(),thread));

        threadController threadController = new threadController();

        ResponseEntity responseEntity = threadController.change("1", thread);
        assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), responseEntity);
        threadDao.close();
    }

    @Test
    public void infoTest(){
        MockedStatic<ThreadDAO> threadDao = Mockito.mockStatic(ThreadDAO.class);
        threadDao.when(() -> ThreadDAO.getThreadById(1))
                .thenReturn(thread);
        threadController threadController = new threadController();

        ResponseEntity responseEntity = threadController.info("1");
        assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), responseEntity);
        threadDao.close();
    }

    @Test
    public void createVoteTest(){
        MockedStatic<ThreadDAO> threadDao = Mockito.mockStatic(ThreadDAO.class);
        threadDao.when(() -> ThreadDAO.getThreadById(1))
                .thenReturn(thread);
        MockedStatic<UserDAO> userDao = Mockito.mockStatic(UserDAO.class);
        userDao.when(() -> UserDAO.Info(any()))
                .thenReturn(user);
        threadDao.when(() -> ThreadDAO.change(any(Vote.class),any()))
                .thenReturn(2);
        threadController threadController = new threadController();
        ResponseEntity responseEntity = threadController.createVote("1",vote);
        Thread responseBody = (Thread) responseEntity.getBody();
        assertEquals(responseEntity.getStatusCode(),HttpStatus.OK);
        assertEquals(responseBody.getId(), thread.getId());
        assertEquals(responseBody.getAuthor(), thread.getAuthor());
        assertEquals(responseBody.getForum(), thread.getForum());
        assertEquals(responseBody.getSlug(), thread.getSlug());
        assertEquals(responseBody.getTitle(), thread.getTitle());
        assertEquals(new Integer(4), thread.getVotes());
        threadDao.close();
        userDao.close();
    }



}